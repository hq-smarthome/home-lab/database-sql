job "database-sql" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  priority = 60

  vault {
    policies = ["job-database-sql"]
  }

  group "sql" {
    count = 1

    network {
      mode = "bridge"
    }

    volume "mysql" {
      type = "host"
      read_only = false
      source = "mysql"
    }

    restart {
      attempts = 10
      interval = "5m"
      delay = "25s"
      mode = "delay"
    }

    service {
      name = "mysql"
      port = "3306"

      connect {
        sidecar_service {}
      }
    }

    task "mysql" {  
      driver = "docker"
      leader = true

      constraint {
        attribute = "${attr.cpu.arch}"
        value = "amd64"
      }

      volume_mount {
        volume = "mysql"
        destination = "/var/lib/mysql"
        read_only = false
      }

      resources {
        cpu    = 500
        memory = 1024
      }

      config {
        image = "mysql:8.0.26"
      }

      template {
        data = <<EOH
          TZ='Europe/Stockholm'

          {{ with secret "jobs/database-sql/tasks/mysql/users/root" }}
          MYSQL_ROOT_PASSWORD={{ index .Data.data "password" }}
          {{ end }}

          {{ with secret "jobs/database-sql/tasks/mysql/users/carbon" }}
          MYSQL_USER={{ index .Data.data "username" }}
          MYSQL_PASSWORD={{ index .Data.data "password" }}
          {{ end }}
        EOH

        destination = "secrets/mysql.env"
        env = true
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
